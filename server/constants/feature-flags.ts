export enum FEATURE_FLAGS {
  CHATBOT = 'chatbot',
  STANDARDIZED_CERTS = 'standardized-certs',
  MUTED_SUBJECT_ALERTS = 'muted-subject-alerts',
  USING_OUR_PLATFORM = 'using-our-platform',
  SESSION_RECAP_DMS = 'session-recap-dms',
  WEEKLY_SUMMARY_ALL_HOURS = 'weekly-summary-all-hours',
  SMS_VERIFICATION = 'sms-verification',
  ALLOW_DMS_TO_PARTNER_STUDENTS = 'allow-dms-to-partner-students',
  PROGRESS_REPORTS = 'progress-reports',
  PROGRESS_REPORTS_VISION_AI = 'progress-reports-vision-ai',
  PAID_TUTORS_PILOT_STUDENT_ELIGIBILITY = 'paid-tutors-pilot-student-eligibility',
  AI_MODERATION = 'ai-moderation',
}
